<?php

require_once dirname(__FILE__).'/functions.php';

function regedit_getAdminSectionMenus(&$url, &$text)
{	
	static $nbMenus=0;
	if( !$nbMenus && !empty($GLOBALS['BAB_SESS_USERID']))
	{
		$url = $GLOBALS['babAddonUrl']."main";
		$text = reg_translate("Configuration editor");
		$nbMenus++;
		return true;
	}
	return false;
}




function regedit_upgrade($version_base,$version_ini)
{
	return true;
}



?>
