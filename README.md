## Editeur de la base de registre ##

Permet l'édition des bases de registres de certains modules. La base de registre est un format de stockage de données mis à disposition par Ovidentia pour les modules n'ayant besoin de stocker que quelques élements. Ces éléments sont enregistrés dans une même table de la base de données d'Ovidentia.