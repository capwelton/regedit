<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
//
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2006 by CANTICO ({@link http://www.cantico.fr})
 */
require_once 'base.php';

require_once $GLOBALS['babInstallPath'] . 'utilit/tree.php';
$addoninfo = bab_getAddonInfosInstance('regedit');
require_once $addoninfo->getPhpPath().'functions.php';


function reg_build()
{
    $tree = new bab_TreeView('regedit');

    $root = $tree->createElement( 'R', 'directory', reg_translate("Root"), '', '');
    $root->setIcon($GLOBALS['babSkinPath'] . 'images/nodetypes/folder.png');

    $root->addAction('add',
            reg_translate("Add"),
            $GLOBALS['babSkinPath'] . 'images/Puces/edit_add.png',
            $GLOBALS['babAddonUrl'].'edit&directory=/',
            '');

    $tree->appendElement($root, NULL);


    function buid_nodeLevel(&$tree, $node, $id, $path)
    {
        $reg = bab_getRegistryInstance();
        $currdir = $reg->getDirectory();
        $reg->changeDirectory($path);


        $i = 1;
        while ($dir = $reg->fetchChildDir()) {
            $element = $tree->createElement( $id.'.'.$i, 'directory', trim($dir,'/'), '', '');
            $element->setIcon($GLOBALS['babSkinPath'] . 'images/nodetypes/folder.png');

            $element->addAction('add',
                                reg_translate('Add'),
                                $GLOBALS['babSkinPath'] . 'images/Puces/edit_add.png',
                                $GLOBALS['babAddonUrl'].'edit&directory='.urlencode($path.$dir),
                                '');
            $element->addAction('rename',
                                reg_translate('Rename'),
                                $GLOBALS['babSkinPath'] . 'images/Puces/edit.gif',
                                $GLOBALS['babAddonUrl'].'move&path='.urlencode($path.$dir),
                                '');

            $tree->appendElement($element, $id);

            buid_nodeLevel($tree, $element, $id.'.'.$i, $path.$dir);

            $reg->changeDirectory($path.$dir);

            $j = 1;
            while ($key = $reg->fetchChildKey()) {
                $child = $tree->createElement( $id.'.'.$i.'_'.$j, 'key', $key, '', '');
                $child->setIcon($GLOBALS['babSkinPath'] . 'images/nodetypes/file.png');

                $value = $reg->getValue($key);

                $valueType = gettype($value);
                switch($valueType) {
                    case 'string':
                    case 'integer':
                    case 'float':
                    case 'double':
                        $displayValue = (string)$value;
                        $child->addAction('edit',
                                reg_translate('Edit'),
                                $GLOBALS['babSkinPath'] . 'images/Puces/edit.gif',
                                $GLOBALS['babAddonUrl'].'edit&key='.urlencode($path.$dir.$key),
                                '');
                        break;

                    case 'boolean':
                        $displayValue = $value ? 'True' : 'False';
                        $child->addAction('edit',
                                reg_translate('Edit'),
                                $GLOBALS['babSkinPath'] . 'images/Puces/edit.gif',
                                $GLOBALS['babAddonUrl'].'edit&key='.urlencode($path.$dir.$key),
                                '');
                        break;

                    case 'Array':
                        $displayValue = @((string) $value);
                        $child->addAction(
                            'edit',
                            reg_translate('Edit'),
                            $GLOBALS['babSkinPath'] . 'images/Puces/edit.gif',
                            $GLOBALS['babAddonUrl'].'edit&key='.urlencode($path.$dir.$key),
                            ''
                        );
                        break;

                    default:
                        $displayValue = @((string) $value);
                        break;
                }

                $child->setInfo(bab_toHtml($displayValue));
                $child->setTooltip($valueType);

                $tree->appendElement($child, $id.'.'.$i);
                $j++;
            }

            $reg->changeDirectory($path);


            $i++;
        }



        $reg->changeDirectory($currdir);
    }

    buid_nodeLevel($tree, $root, 'R' , '/');

    global $babBody;

    $babBody->setTitle(reg_translate("Registry editor"));
    $babBody->babecho($tree->printTemplate());
}


reg_build();
